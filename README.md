# bitcoin-tools
A collection of tools for working with the BitcoinJ API. See [their site](https://bitcoinj.github.io) for more information and documentation. See [their GitHub](https://github.com/bitcoinj/bitcoinj) for the code.

These tools rely upon the BitcoinJ API and will not work alone.

`ScriptUtilities` primarily focuses on making scripts.

`TransactionOutPointWithValue` extends the BitcoinJ `TransactionOutPoint` class to include a value. Objects of this type can be used to represent a UTXO for the sake of e.g., transaction creation.

`TransactionUtilities` attempts to make it easier to create, sign, and print out transactions.

These were tools that have been helpful to me in the past. I have no plans at this time to continue development or make new tools. I may clean up and upload more code I wrote in the past.
