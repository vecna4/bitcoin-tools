/*
 * Copyright 2019 Vecna
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Message;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionInput;
import org.bitcoinj.core.TransactionOutPoint;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptChunk;
import org.bitcoinj.script.ScriptOpCodes;

/**
 * TransactionUtilities is an abstract class that contains generally useful
 * methods for handling transactions, including adding standard inputs and
 * outputs.
 * 
 * @author Vecna
 *
 */
public class TransactionUtilities {

	/**
	 * Adds standard P2PK and/or P2PKH outputs to a transaction.
	 * 
	 * @param tx
	 *            Transaction
	 * @param outputKeys
	 *            List of keys for P2PK outputs
	 * @param outputAddresses
	 *            List of addresses used for P2PKH outputs
	 * @param hashOutputKeys
	 *            List of booleans: true if P2PKH output, false if P2PK
	 * @param outputValues
	 *            List of coins
	 * @param opReturn
	 *            String to be used as an OP_RETURN output. Leave null if no
	 *            OP_RETURN desired.
	 */
	public static void addOutputs(Transaction tx, List<ECKey> outputKeys, List<Address> outputAddresses,
			List<Boolean> hashOutputKeys, List<Coin> outputValues, String opReturn) {
		int keyIndex = 0;
		int addressIndex = 0;
		for (int i = 0; i < hashOutputKeys.size(); i++) {
			if (hashOutputKeys.get(i)) { // P2PKH output - send to address
											// (public key hash)
				tx.addOutput(outputValues.get(i), ScriptUtilities.createP2PKHScript(outputAddresses.get(addressIndex)));
				addressIndex++;
			} else { // P2PK output - send to public key
				tx.addOutput(outputValues.get(i), ScriptUtilities.createP2PKScript(outputKeys.get(keyIndex)));
				keyIndex++;
			}
		}
		if (opReturn != null) {
			ScriptBuilder script = new ScriptBuilder();
			script.addChunk(new ScriptChunk(ScriptOpCodes.OP_RETURN, null));
			script.data(opReturn.getBytes());
			tx.addOutput(Coin.ZERO, script.build());
		}
	}

	/**
	 * Adds and signs standard P2PK and P2PKH inputs of a transaction. Do not use
	 * this method if your transaction will contain any other types of inputs. Each
	 * key corresponds to a list of outpoints that key spends and booleans to
	 * determine that outpoint's nature. This is why there are lists of lists.
	 * 
	 * @param tx
	 *            Transaction
	 * @param inputKeys
	 *            List of keys to sign inputs
	 * @param outPoints
	 *            List of list of outpoints containing the hash and index of each
	 *            UTXO to spend
	 * @param hashInputKeys
	 *            List of list of booleans: true if UTXO to spend is P2PKH, false if
	 *            P2PK
	 */
	public static void addAndSignInputs(Transaction tx, List<ECKey> inputKeys,
			List<ArrayList<TransactionOutPoint>> outPoints, List<ArrayList<Boolean>> hashInputKeys) {
		addInputs(tx, inputKeys, outPoints, hashInputKeys);
		signInputs(tx, inputKeys, hashInputKeys, 0);
	}

	/**
	 * Adds standard P2PK and P2PKH inputs to a transaction. Note: This does not
	 * finalize the transaction. You may still add more inputs, and you will still
	 * need to sign all your inputs. Each key corresponds to a list of outpoints
	 * that key spends and booleans to determine that outpoint's nature. This is why
	 * there are lists of lists.
	 * 
	 * @param tx
	 *            Transaction
	 * @param inputKeys
	 *            List of keys UTXOs to spend are paid to
	 * @param outPoints
	 *            List of list of outpoints containing the hash and index of each
	 *            UTXO to spend
	 * @param hashInputKeys
	 *            List of list of booleans: true if UTXO to spend is P2PKH, false if
	 *            P2PK
	 */
	public static void addInputs(Transaction tx, List<ECKey> inputKeys, List<ArrayList<TransactionOutPoint>> outPoints,
			List<ArrayList<Boolean>> hashInputKeys) {
		for (int i = 0; i < inputKeys.size(); i++) {
			ECKey key = inputKeys.get(i);
			List<TransactionOutPoint> outPointList = outPoints.get(i);
			List<Boolean> hashKeysList = hashInputKeys.get(i);
			for (int j = 0; j < outPointList.size(); j++) {
				TransactionOutPoint outPoint = outPointList.get(j);
				tx.addInput(outPoint.getHash(), outPoint.getIndex(),
						ScriptUtilities.createPayToKeyScript(key, hashKeysList.get(j)));
			}
		}
	}

	/**
	 * Signs standard P2PK and P2PKH inputs of a transaction. Signing should be the
	 * last step in creating the transaction. If you need to add more inputs or
	 * outputs, do not use this method. Each key corresponds to a list of outpoints
	 * that key spends and booleans to determine that outpoint's nature. This is why
	 * there are lists of lists.
	 * 
	 * @param tx
	 *            Transaction
	 * @param keys
	 *            List of keys to sign inputs
	 * @param hashKeys
	 *            List of lists of booleans: true if UTXO to spend is P2PKH, false
	 *            if P2PK
	 * @param inputIndex
	 *            Index of the first input to sign in the transaction using this
	 *            method. This will usually be 0, but if you had other inputs before
	 *            the standard P2PK/P2PKH ones, it will be greater.
	 */
	public static void signInputs(Transaction tx, List<ECKey> keys, List<ArrayList<Boolean>> hashKeys, int inputIndex) {
		List<TransactionInput> inputs = tx.getInputs();
		for (int i = 0; i < keys.size(); i++) {
			ECKey key = keys.get(i);
			List<Boolean> hashKeysList = hashKeys.get(i);
			for (int j = 0; j < hashKeysList.size(); j++) {
				TransactionInput input = inputs.get(inputIndex);
				boolean hashKey = hashKeysList.get(j);
				Script script = ScriptUtilities.createPayToKeyScript(key, hashKey);
				signInput(tx, input, key, script, inputIndex, hashKey);
				inputIndex++;
			}
		}
	}

	/**
	 * Signs a single input.
	 * 
	 * @param tx
	 *            Transaction
	 * @param input
	 *            Input to sign
	 * @param key
	 *            Key with which to sign the input
	 * @param script
	 *            Redeem script (the one that includes the OP_CHECKSIG)
	 * @param index
	 *            Index of the input to sign in the transaction
	 * @param hashKey
	 *            true if the UTXO to spend is P2PKH, false if P2PK
	 * 
	 *            TODO: Remove the need for hashKey. This method will be more
	 *            versatile if we just prepend the signature to whatever script is
	 *            already there.
	 */
	public static void signInput(Transaction tx, TransactionInput input, ECKey key, Script script, int index,
			boolean hashKey) {
		ScriptBuilder sigScript = new ScriptBuilder();
		Sha256Hash hash = tx.hashForSignature(index, script, Transaction.SigHash.ALL, false);
		ECKey.ECDSASignature ecSig = key.sign(hash);
		TransactionSignature txSig = new TransactionSignature(ecSig, Transaction.SigHash.ALL, false);
		sigScript.data(0, txSig.encodeToBitcoin());
		if (hashKey) {
			sigScript.data(1, key.getPubKey());
		}
		Script scriptWithSig = sigScript.build();
		input.setScriptSig(scriptWithSig);
	}

	/**
	 * TODO: Fix this method.
	 * 
	 * Signs a multi-signature input. This method does not appear to work correctly
	 * right now.
	 * 
	 * @param tx
	 *            Transaction
	 * @param keys
	 *            Keys to sign the input
	 * @param input
	 *            Input to sign
	 * @param index
	 *            Index of the input in the transaction
	 * @param script
	 *            Redeem script (the one that includes the OP_CHECKMULTISIG)
	 */
	public static void signMultiSigInput(Transaction tx, ArrayList<ECKey> keys, TransactionInput input, int index,
			Script script) {
		// List<ECKey> keys = keysForMultiSigInputs.get(index);
		ScriptBuilder sigScript = new ScriptBuilder();
		sigScript.addChunk(new ScriptChunk(ScriptOpCodes.OP_0, null));
		// ScriptBuilder outputScriptWithSigs = new ScriptBuilder(script);
		for (int i = 0; i < keys.size(); i++) {
			// for(int i=keys.size()-1; i>=0; i--){
			ECKey key = keys.get(i);
			// script = ScriptUtilities.createP2PKScript(key);
			Sha256Hash hash = tx.hashForSignature(index, script, Transaction.SigHash.ALL, false);
			ECKey.ECDSASignature ecSig = key.sign(hash);
			TransactionSignature txSig = new TransactionSignature(ecSig, Transaction.SigHash.ALL, false);
			sigScript.data(txSig.encodeToBitcoin());
			// outputScriptWithSigs.data(0,txSig.encodeToBitcoin());
			// script = outputScriptWithSigs.build();
			// input.setScriptSig(outputScriptWithSigs.build());
		}
		Script scriptWithSig = sigScript.build();
		input.setScriptSig(scriptWithSig);
	}

	/**
	 * This method prints the transaction details to the console (for
	 * debugging/manual verification purposes). It then prints the transaction's
	 * bytes as a hexadecimal string. Note: For large transactions, the console's
	 * available space is insufficient. For these, print to a file instead.
	 * 
	 * @param tx
	 *            Transaction
	 */
	public static void completeTransaction(Transaction tx) {
		System.out.println(tx);
		Message message = (Message) tx;
		byte[] transactionBytes = message.bitcoinSerialize();
		String transactionHex = DatatypeConverter.printHexBinary(transactionBytes);
		System.out.println(transactionHex);
	}

	/**
	 * This method prints the transaction details to a file (for debugging/manual
	 * verification purposes). It then prints the transaction's bytes as a
	 * hexadecimal string. Note: For large transactions, the console's available
	 * space is insufficient. For these, print to a file instead.
	 * 
	 * @param tx
	 *            Transaction
	 * @param out
	 *            PrintWriter object - the file to print to. If this is null, it
	 *            will print to the console.
	 */
	public static void completeTransaction(Transaction tx, PrintWriter out) {
		if (out == null) {
			completeTransaction(tx);
		} else {
			out.println(tx);
			Message message = (Message) tx;
			byte[] transactionBytes = message.bitcoinSerialize();
			String transactionHex = DatatypeConverter.printHexBinary(transactionBytes);
			out.println(transactionHex);
		}
	}
}
