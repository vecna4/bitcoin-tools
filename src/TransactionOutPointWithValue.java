/*
 * Copyright 2019 Vecna
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.TransactionOutPoint;

/**
 * This class extends the BitcoinJ TransactionOutPoint class, allowing a
 * TransactionOutPoint to be associated with a value. A
 * TransactionOutPointWithValue represents the necessary pieces of a UTXO
 * to meaningfully create a transaction referencing it; however, it is
 * not a linked entity as a TransactionOutput might be.
 *
 * @author Vecna
 *
 *
 */
public class TransactionOutPointWithValue extends TransactionOutPoint {

	private Coin value;

	/**
	 * Initialize a TransactionOutPoint conventionally, with no value associated.
	 * Value is assumed to be 0.
	 * 
	 * @param params
	 * @param index
	 * @param hash
	 */
	public TransactionOutPointWithValue(NetworkParameters params, long index, Sha256Hash hash) {
		super(params, index, hash);
	}

	/**
	 * Initialize a TransactionOutPoint with an associated value.
	 * 
	 * @param params
	 * @param index
	 * @param hash
	 *            a Sha256Hash of the transaction
	 * @param value
	 *            a Coin with the associated value
	 */
	public TransactionOutPointWithValue(NetworkParameters params, long index, Sha256Hash hash, Coin value) {
		this(params, index, hash);
		this.value = value;
	}

	/**
	 * Initialize a TransactionOutPoint with an associated value.
	 * 
	 * @param paramssatoshis
	 * @param index
	 * @param hash
	 *            a string hash of the transaction
	 * @param value
	 *            the value of the TXO
	 */
	public TransactionOutPointWithValue(NetworkParameters params, long index, String hash, long value) {
		this(params, index, Sha256Hash.wrap(hash), Coin.valueOf(value));
	}
	
	public Coin getValue() {
		return value;
	}
	
	public String toString() {
		return "Hash: " + getHash() + "\nIndex: " + getIndex() + "\nValue: " + value;
	}

}
