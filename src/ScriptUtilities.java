/*
 * Copyright 2019 Vecna
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Utils;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptChunk;
import org.bitcoinj.script.ScriptOpCodes;

/**
 * TODO: Add P2SH utilities
 * 
 * ScriptUtilities is an abstract class that contains generally useful methods
 * for handling scripts, including methods to create standard scripts, as well
 * as scripts containing data in the place of keys.
 * 
 * @author Vecna
 *
 */
public class ScriptUtilities {

	/*
	 * P2PK/P2PKH
	 */

	/**
	 * Creates a standard P2PK or P2PKH script that pays to the data provided. The
	 * data is assumed to represent a public key or public key hash, but it may just
	 * be data.
	 * 
	 * @param data
	 *            Byte array representing a public key or public key hash
	 * @param hashKey
	 *            Boolean: true if P2PKH script, false if P2PK
	 * @return P2PK or P2PKH script
	 */
	public static Script createPayToKeyScript(byte[] data, boolean hashKey) {
		if (hashKey) {
			return createP2PKHScript(data);
		} else {
			return createP2PKScript(data);
		}
	}

	/**
	 * Creates a standard P2PK or P2PKH script that pays to the data provided. The
	 * data is assumed to represent a public key or public key hash, but it may just
	 * be data.
	 * 
	 * @param data
	 *            String representing a public key or public key hash
	 * @param hashKey
	 *            Boolean: true if P2PKH script, false if P2PK
	 * @return P2PK or P2PKH script
	 */
	public static Script createPayToKeyScript(String data, boolean hashKey) {
		return createPayToKeyScript(data.getBytes(), hashKey);
	}

	/**
	 * Creates a standard P2PK or P2PKH script that pays to the key provided.
	 * 
	 * @param key
	 * @param hashKey
	 *            Boolean: true if P2PKH script, false if P2PK. Note: Key may
	 *            contain private key data or only public key data. If key does not
	 *            contain private key data, hashKey must be true.
	 * @return P2PK or P2PKH script
	 */
	public static Script createPayToKeyScript(ECKey key, boolean hashKey) {
		if (hashKey) {
			return createP2PKHScript(key.getPubKeyHash());
		} else {
			return createP2PKScript(key.getPubKey());
		}
	}

	/*
	 * P2PKH
	 */

	/**
	 * Creates a standard P2PKH script that pays to the data provided. The data is
	 * assumed to represent a public key hash, but it may just be data.
	 * 
	 * @param data
	 *            String representing a public key hash
	 * @return P2PKH script
	 */
	public static Script createP2PKHScript(String data) {
		return createP2PKHScript(data.getBytes());
	}

	/**
	 * Creates a standard P2PKH script that pays to the data provided. The data is
	 * assumed to represent a public key hash, but it may just be data.
	 * 
	 * @param data
	 *            Byte array representing a public key hash
	 * @return P2PKH script
	 */
	public static Script createP2PKHScript(byte[] data) {
		ScriptBuilder script = new ScriptBuilder();
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_DUP, null));
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_HASH160, null));
		script.data(data);
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_EQUALVERIFY, null));
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_CHECKSIG, null));
		return script.build();
	}

	/**
	 * Creates a standard P2PKH script that pays to the key provided.
	 * 
	 * @param key
	 * @return P2PKH script
	 */
	public static Script createP2PKHScript(ECKey key) {
		return createP2PKHScript(key.getPubKeyHash());
	}

	/**
	 * Creates a standard P2PKH script that pays to the address (public key hash)
	 * provided.
	 * 
	 * @param address
	 * @return P2PKH script
	 */
	public static Script createP2PKHScript(Address address) {
		return createP2PKHScript(address.getHash());
	}

	/*
	 * P2PK
	 */

	/**
	 * Creates a standard P2PK script that pays to the data provided. The data is
	 * assumed to represent a public key, but it may just be data.
	 *
	 * @param data
	 *            Byte array representing a public key
	 * @return P2PK script
	 */
	public static Script createP2PKScript(byte[] data) {
		ScriptBuilder script = new ScriptBuilder();
		script.data(data);
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_CHECKSIG, null));
		return script.build();
	}

	/**
	 * Creates a standard P2PK script that pays to the data provided. The data is
	 * assumed to represent a public key, but it may just be data.
	 *
	 * @param data
	 *            String representing a public key
	 * @return P2PK script
	 */
	public static Script createP2PKScript(String data) {
		return createP2PKScript(data.getBytes());
	}

	/**
	 * Creates a standard P2PK script that pays to the key provided.
	 *
	 * @param key
	 * @return P2PK script
	 */
	public static Script createP2PKScript(ECKey key) {
		return createP2PKScript(key.getPubKey());
	}

	/*
	 * multisig
	 */

	/**
	 * Creates a multi-signature script that pays to M of the keys provided.
	 * 
	 * Due to type erasure in Java, this method must be implemented with a generic
	 * list. A user may use only one type of object in their list.
	 * 
	 * @param keys
	 *            A list of keys. These may be provided as byte arrays, strings, or
	 *            ECKey objects, as long as the list is of only one type.
	 * @param M
	 *            The number of keys required to redeem this output
	 * @return multisig script if it can be created, null otherwise
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Script createMultiSigScript(List keys, int M) {
		List<byte[]> pubKeys;
		if (keys.get(0) instanceof byte[]) {
			pubKeys = (List<byte[]>) keys;
		} else if (keys.get(0) instanceof String) {
			pubKeys = new ArrayList<byte[]>();
			for (Object key : keys) {
				pubKeys.add(((String) key).getBytes());
			}
		} else if (keys.get(0) instanceof ECKey) {
			pubKeys = new ArrayList<byte[]>();
			for (Object key : keys) {
				pubKeys.add(((ECKey) key).getPubKey());
			}
		} else { // incorrect type passed
			return null;
		}
		ScriptBuilder script = new ScriptBuilder();
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_1 + M - 1, null));
		for (byte[] key : pubKeys) {
			script.data(key);
		}
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_1 + pubKeys.size() - 1, null)); // N
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_CHECKMULTISIG, null));
		return script.build();
	}

	/*
	 * OP_RETURN
	 */

	/**
	 * This method accepts a string and returns an OP_RETURN script containing that
	 * string.
	 * 
	 * @param data
	 * @return OP_RETURN script
	 */
	public static Script createOpReturnScript(String data) {
		// data length will be checked in overloaded method
		return createOpReturnScript(data.getBytes());
	}

	/**
	 * This method accepts a byte array and returns an OP_RETURN script containing
	 * that data.
	 * 
	 * @param data
	 * @return OP_RETURN script
	 */
	public static Script createOpReturnScript(byte[] data) {
		if (data.length > 80) {
			throw new IllegalArgumentException("too much OP_RETURN data");
		}
		ScriptBuilder script = new ScriptBuilder();
		script.addChunk(new ScriptChunk(ScriptOpCodes.OP_RETURN, null));
		script.data(data);
		return script.build();
	}

	/*
	 * P2SH
	 */

	/**
	 * This method accepts a script and returns a script paying to the hash of that
	 * script.
	 * 
	 * @param script
	 * @return P2SH script
	 */
	public static Script createP2SHScript(Script script) {
		ScriptBuilder builder = new ScriptBuilder();
		builder.addChunk(new ScriptChunk(ScriptOpCodes.OP_HASH160, null));
		builder.data(Utils.sha256hash160(script.getProgram()));
		builder.addChunk(new ScriptChunk(ScriptOpCodes.OP_EQUAL, null));
		return builder.build();
	}

	/**
	 * Creates a standard P2SH script that pays to the data provided. The data is
	 * assumed to represent a script hash, but it may just be data.
	 * 
	 * @param data
	 *            String representing a script hash
	 * @return P2SH script
	 */
	public static Script createP2SHScript(String data) {
		return createP2SHScript(data.getBytes());
	}

	/**
	 * Creates a standard P2SH script that pays to the data provided. The data is
	 * assumed to represent a script hash, but it may just be data.
	 * 
	 * @param data
	 *            Byte array representing a script hash
	 * @return P2SH script
	 */
	public static Script createP2SHScript(byte[] data) {
		ScriptBuilder builder = new ScriptBuilder();
		builder.addChunk(new ScriptChunk(ScriptOpCodes.OP_HASH160, null));
		builder.data(data);
		builder.addChunk(new ScriptChunk(ScriptOpCodes.OP_EQUAL, null));
		return builder.build();
	}

	/*
	 * Other methods
	 */

	/**
	 * Recreates a script from a string of its hexadecimal bytes.
	 * 
	 * @param scriptString
	 *            Hexadecimal string representing the bytes of a script
	 * @return script
	 */
	public static Script wrap(String scriptString) {
		byte[] scriptBytes = DatatypeConverter.parseHexBinary(scriptString);
		Script script = new Script(scriptBytes);
		return script;
	}
}
